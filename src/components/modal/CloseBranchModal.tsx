import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {ModalContent, BottomModal} from 'react-native-modals';
import {Text, TouchableOpacity} from 'react-native';
import {useTheme} from 'hooks';
import _styles from 'styles/components/CloseBranchModal';

interface IBankDetailsModal {
  open: boolean;
  close: (value: number | string) => void;
}
const BankDetailsModal: React.FC<IBankDetailsModal> = ({open, close}) => {
  const {theme} = useTheme();
  const styles = _styles(theme);

  const onKilometerPress = (value: number | string) => () => close(value);

  const ModalTitle = () => (
    <LinearGradient
      colors={['#FC466B', '#3F5EFB']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={styles.row}>
      <Text style={styles.header}>KM</Text>
      <Text style={styles.header}>نزدیک ترین شعب</Text>
    </LinearGradient>
  );
  return (
    <BottomModal
      visible={open}
      onTouchOutside={close}
      height={0.4}
      width={1}
      onSwipeOut={close}
      modalTitle={ModalTitle()}>
      <ModalContent style={styles.container}>
        <TouchableOpacity onPress={onKilometerPress(3)}>
          <Text style={styles.body}>3 کیلومتر</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onKilometerPress(5)}>
          <Text style={styles.body}>5 کیلومتر</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onKilometerPress(7)}>
          <Text style={styles.body}>7 کیلومتر</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onKilometerPress('all')}>
          <Text style={styles.body}>همه</Text>
        </TouchableOpacity>
      </ModalContent>
    </BottomModal>
  );
};

export default BankDetailsModal;
