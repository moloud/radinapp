import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {ModalContent, BottomModal} from 'react-native-modals';
import {View, Text, Linking, TouchableOpacity, Share} from 'react-native';
import {IBranch} from 'types/bank';
import {useTheme} from 'hooks';
import {EmailSvg, PhoneSvg, DirectionSvg, ShareSvg} from 'assets';
import _styles from 'styles/components/BankDetailsModal';

interface IBankDetailsModal {
  open: boolean;
  close: (destination?: IBranch) => void;
  details?: IBranch;
}
const BankDetailsModal: React.FC<IBankDetailsModal> = ({
  open,
  close,
  details,
}) => {
  const {theme} = useTheme();
  const styles = _styles(theme);

  const onCallPress = () =>
    Linking.openURL(`tel:${details?.branchOfficerPhone}`);

  const onSharePress = () =>
    Share.share({
      title: 'اشتراک گذاری',
      message: `بانک مسکن با نام شعبه ${details?.branchName} واقع در ${details?.branchCity} با کد شهر${details?.branchCitycode} با شماره تماس ${details?.bankingServicesPhone} موجود است.`,
    });

  const onDirectionPress = () => close(details);

  const ModalTitle = () => (
    <LinearGradient
      colors={['#FC466B', '#3F5EFB']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={styles.row}>
      <Text style={styles.header}>{details?.branchCode}</Text>
      <Text style={styles.header}>{details?.branchName}</Text>
    </LinearGradient>
  );
  return (
    <BottomModal
      visible={open}
      onTouchOutside={close}
      height={0.3}
      width={1}
      onSwipeOut={close}
      modalTitle={ModalTitle()}>
      <ModalContent style={styles.container}>
        <View style={styles.rowRight}>
          <Text style={styles.body}>{details?.address}</Text>
          <EmailSvg color="#3F5EFB" />
        </View>
        <TouchableOpacity style={styles.rowRight} onPress={onCallPress}>
          <Text style={styles.body}>{details?.branchOfficerPhone}</Text>
          <PhoneSvg color="#3F5EFB" />
        </TouchableOpacity>
        <View style={styles.rowLeft}>
          <TouchableOpacity
            style={styles.button}
            hitSlop={{top: 0, left: 5, right: 5, bottom: 0}}
            onPress={onDirectionPress}>
            <DirectionSvg color="#3F5EFB" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            hitSlop={{top: 0, left: 5, right: 5, bottom: 0}}
            onPress={onSharePress}>
            <ShareSvg color="#3F5EFB" />
          </TouchableOpacity>
        </View>
      </ModalContent>
    </BottomModal>
  );
};

export default BankDetailsModal;
