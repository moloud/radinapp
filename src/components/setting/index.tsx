import React, {useState} from 'react';
import Modal from 'react-native-modals';
import Slider from '@react-native-community/slider';
import {TouchableOpacity, NativeModules, View, Text} from 'react-native';
import {useTheme, useLayoutAnimation} from 'hooks';
import {useDispatch} from 'react-redux';
import {SunSvg, MoonSvg, PlusSvg, MinusSvg} from 'assets';
import _styles from 'styles/components/Settings';

interface ISetting {
  open: boolean;
  close: () => void;
}

const Settings: React.FC<ISetting> = ({open, close}) => {
  const [maxVolume, setMaxVolume] = useState<number>();
  const [currentVolume, setCurrentVolume] = useState<number>();
  const {isDark} = useTheme();
  const {LayoutAnimation} = useLayoutAnimation();
  const {theme} = useTheme();
  const styles = _styles(theme);
  const dispatch = useDispatch();

  const changeTheme = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    const type = isDark ? 'SET_THEME_LIGHT' : 'SET_THEME_DARK';
    dispatch({type});
  };
  // voice handling
  NativeModules.VolumeController.getMaxVolume((err: null, max: number) => {
    if (err) {
      // handle err
    }
    setMaxVolume(max);
  });

  NativeModules.VolumeController.getVolume((err: null, current: number) => {
    if (err) {
      // handle err
    }
    setCurrentVolume(current);
  });

  const raiseVoice = () =>
    NativeModules.VolumeController.raiseVolume((err: null, current: number) => {
      if (err) {
        // handle err
      }
      setCurrentVolume(current);
    });

  const lowerVoice = () =>
    NativeModules.VolumeController.lowerVolume((err: null, current: number) => {
      if (err) {
        // handle err
      }
      setCurrentVolume(current);
    });

  const onChangeVoice = (volume: number) => {
    NativeModules.VolumeController.changeVolume(volume);
    setCurrentVolume(volume);
  };

  const SliderContainer = (
    <View style={styles.row}>
      <TouchableOpacity onPress={lowerVoice}>
        <MinusSvg color="#fa163f" />
      </TouchableOpacity>
      <Slider
        style={styles.slider}
        minimumValue={0}
        maximumValue={maxVolume}
        minimumTrackTintColor="#000000"
        maximumTrackTintColor="#000000"
        value={currentVolume}
        onValueChange={onChangeVoice}
      />
      <TouchableOpacity onPress={raiseVoice}>
        <PlusSvg />
      </TouchableOpacity>
    </View>
  );

  const ChangeTheme = (
    <TouchableOpacity onPress={changeTheme} activeOpacity={0.7}>
      <View style={styles.row}>
        {isDark ? (
          <>
            <SunSvg color="#f7f700" />
            <Text style={styles.text}>حالت روز</Text>
          </>
        ) : (
          <>
            <MoonSvg color="#363738" />
            <Text style={styles.text}>حالت شب</Text>
          </>
        )}
      </View>
    </TouchableOpacity>
  );
  return (
    <Modal rounded visible={open} onTouchOutside={close}>
      <View style={styles.container}>
        {SliderContainer}
        {ChangeTheme}
      </View>
    </Modal>
  );
};

export default Settings;
