export {default as ActivityIndicator} from './loading/ActivityIndicator';
export {default as FadeInView} from './wrapper/FadeInView';
export {default as Wrapper} from './wrapper/Wrapper';
export {default as Settings} from './setting';
export {default as BankDetailsModal} from './modal/BankDetails';
export {default as CloseBranchModal} from './modal/CloseBranchModal';
