/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef} from 'react';
import {View, Text, ActivityIndicator, Animated, Easing} from 'react-native';
import {useTheme} from 'hooks';
import _styles from 'styles/components/Wrapper';

const Wrapper = (props) => {
  const {theme} = useTheme();
  const styles = _styles(theme);
  const [screenLoaded, setScreenLaded] = useState(false);
  const containerScaleAnimation = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    requestAnimationFrame(() => {
      setScreenLaded(true);
    });
  }, []);

  useEffect(() => {
    Animated.timing(containerScaleAnimation, {
      toValue: 0,
      useNativeDriver: true,
      duration: 700,
      delay: 50,
      easing: Easing.bezier(0, 1, 0, 1),
    }).start();
  }, [containerScaleAnimation]);

  const topTranslateYValue = containerScaleAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -2500],
  });

  const innerHeader = () => {
    return (
      <>
        <View style={[styles.sideTopContainer, {alignItems: 'flex-start'}]}>
          {props.renderLeft && props.renderLeft()}
        </View>
        <View style={styles.centerTopContainer}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View style={[styles.sideTopContainer, {alignItems: 'flex-end'}]}>
          {props.renderRight && props.renderRight()}
        </View>
      </>
    );
  };

  if (!screenLoaded) {
    return (
      <View style={styles.onLoadingContainer}>
        <ActivityIndicator
          color={props.activityIndicatorColor || theme.$primary}
        />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.absoulteTop,
          {transform: [{translateY: topTranslateYValue}]},
        ]}
      />
      <Animated.View
        style={[
          styles.topContainer,
          {transform: [{translateY: topTranslateYValue}]},
        ]}>
        {innerHeader()}
      </Animated.View>
      <View style={styles.body}>{props.children}</View>
    </View>
  );
};

export default Wrapper;
