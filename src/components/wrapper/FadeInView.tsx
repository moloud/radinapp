import React, {useRef, useEffect, useCallback} from 'react';
import {Animated, Easing} from 'react-native';

const FadeInView = ({children, style, slideIn = true}) => {
  const animatedValue = useRef(new Animated.Value(0)).current;

  const startAnimation = useCallback(() => {
    Animated.timing(animatedValue, {
      toValue: 1,
      useNativeDriver: true,
      duration: 600,
      easing: Easing.out(Easing.bezier(1, 0, 0.7, 1)),
    }).start();
  }, [animatedValue]);

  useEffect(() => {
    startAnimation();
  }, [animatedValue, startAnimation]);

  const containerTranslateYAnim = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [50, 0],
  });

  const containerOpacityAnim = animatedValue.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, 0.2, 1],
  });

  return (
    <Animated.View
      style={[
        style,
        {
          transform: [{translateY: slideIn ? containerTranslateYAnim : 0}],
          opacity: containerOpacityAnim,
        },
      ]}>
      {children}
    </Animated.View>
  );
};

export default FadeInView;
