/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image, Text} from 'react-native';
import {SimpleLoading} from 'assets';
import {useTheme} from 'hooks';
import _styles from 'styles/components/ActivityIndicator';

const ActivityIndicator: React.FC<{title: string}> = ({title}) => {
  const {theme} = useTheme();
  const styles = _styles(theme);

  return (
    <View style={styles.container}>
      <Image source={SimpleLoading} style={styles.loading} />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

export default ActivityIndicator;
