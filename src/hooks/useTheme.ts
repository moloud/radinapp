import {useSelector} from 'react-redux';

function useTheme() {
  const theme = useSelector((state) => state.appTheme.theme);
  const isDark = useSelector((state) => state.appTheme.is_dark);
  return {theme, isDark};
}
export default useTheme;
