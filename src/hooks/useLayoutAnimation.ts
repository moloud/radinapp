import {useEffect} from 'react';
import {UIManager, LayoutAnimation, Platform} from 'react-native';

function useLayoutAnimation() {
  useEffect(() => {
    if (
      Platform.OS === 'android' &&
      UIManager.setLayoutAnimationEnabledExperimental
    ) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }, []);
  return {LayoutAnimation};
}

export default useLayoutAnimation;
