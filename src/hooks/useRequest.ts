import {useState} from 'react';
import qs from 'qs';
import Axios from 'utils/fetcher';
import {AxiosRequestConfig} from 'axios';
import config from 'utils/env';

interface IGetConfig {
  url: string;
  useToken?: boolean;
  onSuccess?(data: any): void;
  onFailed?(err: any): void;
  method: 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'GET';
}
const useFetch = (request: IGetConfig) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const axiosConfig: AxiosRequestConfig = {};

  const call = (params: any = {}) => {
    //comepete axios config
    axiosConfig.baseURL = config.serverURL;
    axiosConfig.url = `${request.url}?${qs.stringify(params)}`;
    axiosConfig.method = request.method;
    // add token to axios header

    setLoading(true);
    Axios({...axiosConfig})
      .then((res) => {
        if (res.status === 200)
          request?.onSuccess && request.onSuccess(res?.data);
        setError(false);
      })
      .catch((err) => {
        request?.onFailed && request.onFailed(err);
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return {call, loading, error};
};

export default useFetch;
