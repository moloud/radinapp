export interface IBranch {
  id: number;
  branchName: string;
  branchManagementName: string;
  branchCity: string;
  branchCode: number;
  latitude: number;
  longitude: number;
  bankingServicesPhone: string;
  branchOfficerPhone: string;
  branchCitycode: string;
  postalCode: string;
  address: string;
  type: number;
}
