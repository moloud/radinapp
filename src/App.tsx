/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import store from './redux/store';
import BankList from 'screens/banks/List';
import BankLocations from 'screens/banks/Locations';

import 'react-native-gesture-handler';

const Stack = createStackNavigator();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="BankList" component={BankList} />
          <Stack.Screen name="BankLocations" component={BankLocations} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
