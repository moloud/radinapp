import React from 'react';
import Svg, {Path} from 'react-native-svg';

const Location = ({width = 20, height = 20, color = '#2ECB72'}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 10 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M4.99998 0.333344C2.41998 0.333344 0.333313 2.42001 0.333313 5.00001C0.333313 8.50001 4.99998 13.6667 4.99998 13.6667C4.99998 13.6667 9.66665 8.50001 9.66665 5.00001C9.66665 2.42001 7.57998 0.333344 4.99998 0.333344ZM4.99998 6.66668C4.07998 6.66668 3.33331 5.92001 3.33331 5.00001C3.33331 4.08001 4.07998 3.33334 4.99998 3.33334C5.91998 3.33334 6.66665 4.08001 6.66665 5.00001C6.66665 5.92001 5.91998 6.66668 4.99998 6.66668Z"
        fill={color}
      />
    </Svg>
  );
};

export default Location;
