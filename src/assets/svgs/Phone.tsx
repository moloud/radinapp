import React from 'react';
import Svg, {Path} from 'react-native-svg';

const PhoneSvg = ({width = 26, height = 26, color = 'white'}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M20.8031 20.2417C21.6005 20.906 21.6007 21.9731 20.7932 22.6335L18.8334 24.2363C18.0305 24.893 16.5733 25.1976 15.5961 24.8629C15.5961 24.8629 11.6473 24.1186 6.75962 19.2329C1.87193 14.3472 1.12727 10.4001 1.12727 10.4001C0.816549 9.4155 1.0935 7.97125 1.75414 7.16414L3.35765 5.2051C4.01459 4.40251 5.0853 4.39742 5.75043 5.19525L8.17078 8.09846C8.61334 8.62932 8.6638 9.52193 8.27764 10.1009L7.20212 11.7135C7.20212 11.7135 7.21514 14.3805 9.41461 16.579C11.6141 18.7776 14.2821 18.7906 14.2821 18.7906L15.8954 17.7155C16.4707 17.3321 17.3716 17.3833 17.8987 17.8223L20.8031 20.2417Z"
        stroke={color}
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M25 13C25 6.37258 19.6274 1 13 1"
        stroke={color}
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M19.6 13C19.6 9.35491 16.6451 6.39999 13 6.39999"
        stroke={color}
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};

export default PhoneSvg;
