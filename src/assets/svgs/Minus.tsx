import React from 'react';
import Svg, {Path} from 'react-native-svg';

const Minus = ({width = 20, height = 20, color = '#2ECB72'}) => {
  return (
    <Svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 512"
      width={width}
      height={height}
      style="enable-background:new 0 0 512 512;"
      xml="preserve">
      <Path
        d="M426.667,170.667H85.333C38.272,170.667,0,208.939,0,256s38.272,85.333,85.333,85.333h341.333
			C473.728,341.333,512,303.061,512,256S473.728,170.667,426.667,170.667z"
        fill={color}
      />
    </Svg>
  );
};

export default Minus;
