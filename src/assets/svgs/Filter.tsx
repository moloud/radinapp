import React from 'react';
import Svg, {Path} from 'react-native-svg';

const Filter = ({width = 18, height = 22, color = 'white'}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 18 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M0.600006 4.4V2C0.600006 2 4.60801 0.800003 9.00001 0.800003C13.392 0.800003 17.4 2 17.4 2V4.4L11.4 12.836V21.2C11.4 21.2 9.93601 21.092 8.70001 20.492C7.46401 19.892 6.60001 18.8 6.60001 18.8V12.836L0.600006 4.4Z"
        fill={color}
      />
    </Svg>
  );
};

export default Filter;
