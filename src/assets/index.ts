//animations
export const SimpleLoading = require('./animations/loading.gif');

export const fonts = {
  bold: 'IRANSansWeb(FaNum)_Bold',
  light: 'IRANSansWeb(FaNum)_Light',
};

export {default as PhoneSvg} from './svgs/Phone';
export {default as SunSvg} from './svgs/Sun';
export {default as MoonSvg} from './svgs/Moon';
export {default as SettingsSvg} from './svgs/Settings';
export {default as PlusSvg} from './svgs/Plus';
export {default as MinusSvg} from './svgs/Minus';
export {default as LocationSvg} from './svgs/Location';
export {default as EmailSvg} from './svgs/Email';
export {default as DirectionSvg} from './svgs/Direction';
export {default as ShareSvg} from './svgs/Share';
export {default as FilterSvg} from './svgs/Filter';
