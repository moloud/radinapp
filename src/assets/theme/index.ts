export const theme_dark = {
  $primary: '#1F203E',
  $white: '#171717',
  $dark: '#F0F0F5',
  $gray: '#E6E6EB',
  $grayLight: '#BABBC2',
  $grayDarker: '#F0F0F5',
  $grayLighter: '#363738',
  $black: '#ffffff',
};
export const theme_light = {
  $primary: '#1F203E',
  $white: '#ffffff',
  $grayLighter: '#e3e3e8',
  $grayLight: '#E6E6EB',
  $gray: '#BABBC2',
  $grayDarker: '#8D8D94',
  $dark: '#33383D',
  $black: '#000000',
};
export default theme_light;
