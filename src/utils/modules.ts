import {IBranch} from 'types/bank';
interface ICoords {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}
export const getRoutePoints = (origin: ICoords, destination: IBranch) => {
  console.log('origin destination', origin, destination);
  const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=AIzaSyAp0MiiP5SqsjOnhaTHbOdsp5aQAVEfU1U&mode=driving`;
  console.log('url', url);
  let coords: any = [];
  fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {
      console.log('routes', responseJson.routes);
      if (responseJson.routes.length) {
        coords = decode(responseJson.routes[0].overview_polyline.points); // definition below;
      }
    })
    .catch((e) => {
      console.log('getRouteErr', e);
    });
  return coords;
};

const decode = (t) => {
  for (
    var n,
      o,
      u = 0,
      l = 0,
      r = 0,
      d = [],
      h = 0,
      i = 0,
      a = null,
      c = Math.pow(10, 5);
    u < t.length;

  ) {
    (a = null), (h = 0), (i = 0);
    do (a = t.charCodeAt(u++) - 63), (i |= (31 & a) << h), (h += 5);
    while (a >= 32);
    (n = 1 & i ? ~(i >> 1) : i >> 1), (h = i = 0);
    do (a = t.charCodeAt(u++) - 63), (i |= (31 & a) << h), (h += 5);
    while (a >= 32);
    (o = 1 & i ? ~(i >> 1) : i >> 1),
      (l += n),
      (r += o),
      d.push([l / c, r / c]);
  }
  return (d = d.map(function (t) {
    return {
      latitude: t[0],
      longitude: t[1],
    };
  }));
};

export const distance = (origin: ICoords, destination: IBranch) => {
  var R = 6371; // Radius of the earth in km
  var dLat = ((destination.latitude - origin.latitude) * Math.PI) / 180; // Javascript functions in radians
  var dLon = ((destination.longitude - origin.longitude) * Math.PI) / 180;
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos((origin.latitude * Math.PI) / 180) *
      Math.cos((destination.latitude * Math.PI) / 180) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
};
