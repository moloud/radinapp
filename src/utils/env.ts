const env = process.env.ENV || 'DEVELOPMENT_LOCAL';

interface IBaseUrl {
  serverURL: string;
}
interface Iconfig {
  DEVELOPMENT_LOCAL: {serverURL: string};
}
const config = {
  DEVELOPMENT_LOCAL: {
    serverURL: 'http://radin.radin.tech/api/',
  },
};

const finalConfig = {
  ...config[env],
};

export default finalConfig;
