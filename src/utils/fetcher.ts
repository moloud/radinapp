import axios, {AxiosResponse, AxiosError} from 'axios';

const instance = axios.create();
instance.interceptors.response.use(
  (response: AxiosResponse) => response,
  (error: AxiosError) => console.log('server error', error),
);
export default instance;
