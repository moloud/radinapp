import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.$white,
      padding: 16,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    row: {
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16,
    },
    text: {
      textAlign: 'right',
      color: theme.$dark,
      fontSize: 13,
      fontFamily: fonts.bold,
      padding: 16,
    },
    slider: {
      width: 200,
      height: 40,
    },
  });
