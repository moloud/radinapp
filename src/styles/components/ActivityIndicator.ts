import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 150
    },
    loading: {
      width: 140,
      height: 140,
    },

    title: {
      fontSize: 15,
      fontFamily: fonts.bold,
      color: theme.$dark,
      textAlign: 'center',
    },
  });
