import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.$white,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    row: {
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      padding: 16,
    },
    header: {
      fontSize: 15,
      fontFamily: fonts.bold,
      color: '#fff',
    },
    body: {
      textAlign: 'right',
      color: theme.$dark,
      fontSize: 13,
      fontFamily: fonts.bold,
      padding: 16,
    },
    rowRight: {
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16,
    },
    rowLeft: {
      width: '100%',
      justifyContent: 'flex-start',
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16,
    },
    button: {
      paddingHorizontal: 5,
    },
  });
