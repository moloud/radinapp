import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.$white,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    header: {
      fontSize: 15,
      fontFamily: fonts.bold,
      color: '#fff',
    },
    row: {
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      padding: 16,
    },
    body: {
      textAlign: 'right',
      color: theme.$dark,
      fontSize: 13,
      fontFamily: fonts.bold,
      padding: 16,
    },
  });
