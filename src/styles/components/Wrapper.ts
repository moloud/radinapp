import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    onLoadingContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: theme.$white,
    },
    container: {
      flex: 1,
      backgroundColor: theme.$grayLighter,
    },
    absoulteTop: {
      position: 'absolute',
      backgroundColor: theme.$primary,
      height: '15%',
      width: '100%',
      top: 0,
      borderBottomLeftRadius: 11,
      borderBottomRightRadius: 11,
      zIndex: 0,
    },
    topContainer: {
      flex: 1,
      flexDirection: 'row',
      paddingHorizontal: '5%',
      alignItems: 'center',
    },
    sideTopContainer: {
      flex: 3,
    },

    centerTopContainer: {
      flex: 6,
      justifyContent: 'center',
      alignItems: 'center',
    },
    body: {
      flex: 9,
    },
    defaultContainerStyle: {
      borderRadius: 11,
      backgroundColor: theme.$white,
      elevation: 50,
    },
    headerContainer: {
      flex: 1,
      backgroundColor: theme.$primary,
      borderBottomLeftRadius: 11,
      borderBottomRightRadius: 11,
    },
    title: {
      fontSize: 15,
      fontFamily: fonts.bold,
      color: '#BABBC2',
      textAlign: 'center',
    },
  });
