import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      marginHorizontal: '5%',
      marginVertical: 8,
      borderRadius: 13,
      backgroundColor: theme.$white,
      minHeight: 160,
      overflow: 'hidden',
      elevation: 6,
      borderWidth: 1,
      borderColor: theme.$grayLighter,
      paddingBottom: 10,
    },
    row: {
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      padding: 16,
    },
    header: {
      fontSize: 15,
      fontFamily: fonts.bold,
      color: '#fff',
    },
    body: {
      textAlign: 'right',
      color: theme.$dark,
      fontSize: 13,
      fontFamily: fonts.bold,
      padding: 16,
    },
    rowRight: {
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 16,
    },
  });
