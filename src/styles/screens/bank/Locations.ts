import {StyleSheet} from 'react-native';
import {fonts} from 'assets';

export default (theme: any) =>
  StyleSheet.create({
    container: {
      marginHorizontal: '5%',
      marginVertical: 8,
      borderRadius: 13,
      backgroundColor: theme.$white,
      height: '90%',
      overflow: 'hidden',
      elevation: 6,
      borderWidth: 1,
      borderColor: theme.$grayLighter,
      paddingBottom: 10,
    },
    mapContainer: {
      ...StyleSheet.absoluteFillObject,
    },
    currentRegion: {
      width: 20,
      height: 20,
      borderRadius: 10,
      borderWidth: 5,
      borderColor: '#2ECB72',
      backgroundColor: '#fff',
    },
    coordinate: {
      width: 20,
      height: 20,
      borderRadius: 10,
      borderWidth: 5,
      borderColor: '#4FA5FB',
      backgroundColor: '#fff',
    },
    kilometerCard: {
      borderRadius: 11,
      width: '90%',
      height: 50,
      marginTop: 10,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.$white,
      elevation: 10,
      borderColor: theme.$grayLighter,
      borderWidth: 1,
      alignSelf: 'center',
      overflow: 'hidden',
    },
    kilometerText: {
      textAlign: 'right',
      color: theme.$dark,
      fontSize: 13,
      fontFamily: fonts.bold,
      padding: 16,
    },
  });
