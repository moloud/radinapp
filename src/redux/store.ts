import {createStore} from 'redux';
//import rootReducer and rootSaga
import rootReducer from './reducers';

//export store
const store = createStore(rootReducer);
export default store;
