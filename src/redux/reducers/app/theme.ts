import {theme_dark, theme_light} from 'assets/theme';
import {IAction} from 'types/action';

const initialState = {
  is_dark: false,
  theme: {
    ...theme_light,
  },
};

function reducer(state = initialState, action: IAction) {
  switch (action.type) {
    case 'SET_THEME_DARK':
      return {...state, theme: theme_dark, is_dark: true};
    case 'SET_THEME_LIGHT':
      return {...state, theme: theme_light, is_dark: false};
    default:
      return state;
  }
}

export default reducer;
