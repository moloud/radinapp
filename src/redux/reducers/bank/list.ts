import {IAction} from 'types/action';

const initialState = {
  branches: [],
};

function reducer(state = initialState, action: IAction) {
  switch (action.type) {
    case 'CHANGE_BANK_LIST':
      return {...state, ...action.payload};
    default:
      return state;
  }
}

export default reducer;
