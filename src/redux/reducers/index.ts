import {combineReducers} from 'redux';
import bankList from './bank/list';
import appTheme from './app/theme';

export default combineReducers({bankList, appTheme});
