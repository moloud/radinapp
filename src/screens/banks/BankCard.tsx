import React from 'react';
import {View, Text} from 'react-native';
import {FadeInView} from 'components';
import {IBranch} from 'types/bank';
import {useTheme} from 'hooks';
import LinearGradient from 'react-native-linear-gradient';
import {PhoneSvg} from 'assets';
import _styles from 'styles/screens/bank/BankCard';

const BankCard: React.FC<{info: IBranch}> = ({info}) => {
  const {theme} = useTheme();
  const styles = _styles(theme);
  return (
    <FadeInView style={styles.container}>
      <LinearGradient
        colors={['#FC466B', '#3F5EFB']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={styles.row}>
        <Text style={styles.header}>{info?.branchCode}</Text>
        <Text style={styles.header}>{info?.branchName}</Text>
      </LinearGradient>
      <Text style={styles.body}>{info?.address}</Text>
      <View style={styles.rowRight}>
        <Text style={styles.body}>شماره دفتر - {info?.branchOfficerPhone}</Text>
        <PhoneSvg color="#008C6E" />
      </View>
    </FadeInView>
  );
};
export default BankCard;
