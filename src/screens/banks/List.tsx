import React, {useEffect, useState} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {useRequest} from 'hooks';
import {useSelector, useDispatch} from 'react-redux';
import {ActivityIndicator, Wrapper, Settings} from 'components';

import {SettingsSvg, LocationSvg} from 'assets';
import {IBranch} from 'types/bank';
import BankCard from './BankCard';

// TODO add i18n for texts
type IRootStackParamList = {
  BankList: undefined;
  BankLocations: undefined;
};

const BankList: React.FC<{
  navigation: StackScreenProps<IRootStackParamList, 'BankList'>;
}> = ({navigation}) => {
  const [openSetting, setOpenSetting] = useState<boolean>(false);
  const dispatch = useDispatch();

  //select from store
  const branches = useSelector((state: any) => state.bankList.branches);

  // http request
  const banks = useRequest({
    url: 'bank-branches',
    useToken: false,
    method: 'GET',
    onSuccess: (data) => dispatch({type: 'CHANGE_BANK_LIST', payload: data}),
  });

  useEffect(() => {
    banks.call();
  }, []);

  const onOpenSetting = () => setOpenSetting(true);
  const onCloseSetting = () => setOpenSetting(false);
  const goToLocation = () => navigation.navigate('BankLocations');
  // ui
  const renderBankItem = ({item}: {item: IBranch}) => <BankCard info={item} />;

  if (banks.loading)
    return <ActivityIndicator title="در حال یافتن لیست شعب بانک ها ... " />;
  return (
    <>
      <Settings open={openSetting} close={onCloseSetting} />
      <Wrapper
        title="شعب"
        useDefaultStyleContainer={false}
        useOnlyHeader
        renderRight={() => (
          <TouchableOpacity onPress={goToLocation} activeOpacity={0.7}>
            <LocationSvg color="#BABBC2" />
          </TouchableOpacity>
        )}
        renderLeft={() => (
          <TouchableOpacity onPress={onOpenSetting} activeOpacity={0.7}>
            <SettingsSvg color="#BABBC2" />
          </TouchableOpacity>
        )}>
        <FlatList
          data={branches}
          keyExtractor={(item) => item?.id?.toString()}
          renderItem={renderBankItem}
        />
      </Wrapper>
    </>
  );
};

export default BankList;
