import React, {useState, useEffect} from 'react';
import MapViewDirections from 'react-native-maps-directions';
import Geolocation from '@react-native-community/geolocation';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {useSelector} from 'react-redux';
import {View, TouchableOpacity, Text} from 'react-native';
import {
  Wrapper,
  BankDetailsModal,
  CloseBranchModal,
  Settings,
  FadeInView,
} from 'components';
import {useTheme} from 'hooks';
import {IBranch} from 'types/bank';
import {distance} from 'utils/modules';
import {SettingsSvg, FilterSvg} from 'assets';
import _styles from 'styles/screens/bank/Locations';
import {mapDarkMode, mapStandard} from 'styles/components/mapStyle';

interface ICoords {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}
const BankLocations: React.FC = () => {
  const [openSetting, setOpenSetting] = useState<boolean>(false);
  const branches = useSelector((state: any) => state.bankList.branches);
  const [openFilter, setOpenFilter] = useState<boolean>(false);
  const [openDetails, setOpenDetails] = useState<boolean>(false);
  const [kilometer, setKilometer] = useState<number | string>();
  const [closeLocations, setCloseLocations] = useState<IBranch[]>([]);
  const [details, setDetails] = useState<IBranch>();
  const [destination, setDestination] = useState<IBranch>();
  const [region, setRegion] = useState<ICoords>({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 5,
    longitudeDelta: 5,
  });
  const {theme, isDark} = useTheme();
  const styles = _styles(theme);

  const getCurrentLocation = async () => {
    Geolocation.getCurrentPosition(
      (position) => {
        setRegion({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 5,
          longitudeDelta: 5,
        });
      },
      (error) => console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  };

  const getCloseDistance = () => {
    let closeDisance: IBranch[] = [];
    if (kilometer === 'all') {
      setCloseLocations([...branches]);
      return;
    }
    branches.map((coords: IBranch) => {
      if (distance(region, coords) < kilometer) closeDisance.push(coords);
      return coords;
    });
    setCloseLocations(closeDisance);
  };

  useEffect(() => {
    getCurrentLocation();
  }, []);

  useEffect(() => {
    if (kilometer) {
      getCloseDistance();
    }
  }, [region, kilometer]);

  const onOpenDetails = (branchDetails: IBranch) => () => {
    setOpenDetails(true);
    setDetails(branchDetails);
  };

  const onCloseDetails = (destinationBranch?: IBranch) => {
    setOpenDetails(false);
    setDestination(destinationBranch);
  };

  const onOpenSetting = () => setOpenSetting(true);
  const onCloseSetting = () => setOpenSetting(false);
  const onOpenFilter = () => setOpenFilter(true);
  const onCloseFilter = (value?: number | string) => {
    if (value) setKilometer(value);
    setOpenFilter(false);
  };

  return (
    <Wrapper
      title="اطراف من"
      useDefaultStyleContainer={false}
      useOnlyHeader
      renderRight={() => (
        <TouchableOpacity onPress={onOpenFilter} activeOpacity={0.7}>
          <FilterSvg color="#BABBC2" />
        </TouchableOpacity>
      )}
      renderLeft={() => (
        <TouchableOpacity onPress={onOpenSetting} activeOpacity={0.7}>
          <SettingsSvg color="#BABBC2" />
        </TouchableOpacity>
      )}>
      <Settings open={openSetting} close={onCloseSetting} />
      <BankDetailsModal
        open={openDetails}
        close={onCloseDetails}
        {...{details}}
      />
      <CloseBranchModal open={openFilter} close={onCloseFilter} />
      {kilometer && kilometer !== 'all' && (
        <FadeInView style={styles.kilometerCard}>
          <Text style={styles.kilometerText}>اطراف {kilometer} کیلومتر</Text>
        </FadeInView>
      )}
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.mapContainer}
          initialRegion={region}
          customMapStyle={isDark ? mapDarkMode : mapStandard}
          zoomEnabled>
          <Marker
            coordinate={{
              latitude: region.latitude,
              longitude: region.longitude,
            }}>
            <View style={styles.currentRegion} />
          </Marker>
          {destination?.id ? (
            <>
              <Marker
                coordinate={destination}
                key={destination.id}
                onPress={onOpenDetails(destination)}>
                <View style={styles.coordinate} />
              </Marker>
              <MapViewDirections
                origin={region}
                destination={destination}
                apikey="AIzaSyAp0MiiP5SqsjOnhaTHbOdsp5aQAVEfU1U"
                strokeWidth={3}
                strokeColor="hotpink"
              />
            </>
          ) : (
            closeLocations.map((coords: IBranch) => (
              <Marker
                coordinate={coords}
                key={coords.id}
                onPress={onOpenDetails(coords)}>
                <View style={styles.coordinate} />
              </Marker>
            ))
          )}
        </MapView>
      </View>
    </Wrapper>
  );
};

export default BankLocations;
