module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          hooks: './src/hooks',
          utils: './src/utils',
          screens: './src/screens',
          components: './src/components',
          assets: './src/assets',
          types: './src/types',
          styles: './src/styles',
        },
      },
    ],
  ],
};
