package com.radinapp.VolumeController;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class VolumeControllerModule extends ReactContextBaseJavaModule {
    private AudioManager audioManager;
    private ReactApplicationContext context;

    public VolumeControllerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
        audioManager = (AudioManager) reactContext.getSystemService(Context.AUDIO_SERVICE);
    }

    @NonNull
    @Override
    public String getName() {
        return "VolumeController";
    }

    @ReactMethod
    public void getVolume(Callback cb) {
        float currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        cb.invoke(null, currentVolume);
    }

    @ReactMethod
    public void getMaxVolume(Callback cb) {
        float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        cb.invoke(null, maxVolume);
    }

    @ReactMethod
    public void raiseVolume(Callback cb){
        audioManager.adjustVolume(AudioManager.ADJUST_RAISE,AudioManager.FLAG_PLAY_SOUND);
        cb.invoke(null, audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @ReactMethod
    public void lowerVolume(Callback cb){
        audioManager.adjustVolume(AudioManager.ADJUST_LOWER,AudioManager.FLAG_PLAY_SOUND);
        cb.invoke(null, audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }

    @ReactMethod
    public void changeVolume(float volume) {
        try {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (volume), AudioManager.FLAG_PLAY_SOUND);
        } catch (Exception e) {
            Log.e("VoiceModule", "Error Setting Volume", e);
        }
    }

}
